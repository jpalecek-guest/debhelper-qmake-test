#include <iostream>

#define QUOTE2(p) #p
#define QUOTE(p) QUOTE2(p)

int main()
{
    std::cout << "Hello world " QUOTE(EXTRA_ARGS);
    return 0;
}
