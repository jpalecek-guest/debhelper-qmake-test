CONFIG -= qt
CONFIG += cmdline c++2b

TARGET = hello

DEFINES += EXTRA_ARGS=$${QMAKE_EXTRA_ARGS}

yadda {
	SOURCES = main.cpp
}

target.path = $${INSTALL_PREFIX}/bin

INSTALLS += target

